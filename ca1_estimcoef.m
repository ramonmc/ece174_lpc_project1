% ca1_estimcoef() - Estimate coefficient 1(See computer assigment 1).
%
% Input:
%   yblock      - Qunatized signal [Dimension [Number of blocks,Number of points ]
%
% Optional input:
%   order      - Order used to estimate the coefficients
%
% Outputs:
%   coef_a    - Coefficients. Dimension [Number of blocks, order]
%   res       - Residuals between the signal reconstructed with the
%               coefficients and the quantized signal (input)
%   resMSE    - Mean Suqered error of the residuals
%
%
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function [coef_a,res,resMSE] = ca1_estimcoef(yblock,varargin)
coef_a = [];

try
    options = varargin;
    if ~isempty( varargin ),
        for i = 1:2:numel(options)
            g.(options{i}) = options{i+1};
        end
    else g= []; end;
catch
    disp('ca1_estimcoef() error: calling convention {''key'', value, ... } error'); return;
end;
try g.order;     catch, g.order   = 10;   end;

blklength =  size(yblock,2);

% % Build index matrix from Toeplitz Matrix for each block
% Amatindx = toeplitz(g.order:(g.order+blklength-1),flip(1:g.order));

% Creating matrix of overlapped segments == order
coef_a     = zeros(size(yblock,1),g.order);

for iblock = 1:size(yblock,1)
    yblock_ext = zeros(1,size(yblock,2)+g.order-1);
    if iblock ==1
        yblock_ext(g.order+1:end) = yblock(1,1:end-1);
    else
        yblock_ext(1:g.order)     = yblock(iblock-1,end-(g.order-1):end);
        yblock_ext(g.order+1:end) = yblock(iblock,1:end-1);
    end
  
    Amat = toeplitz(yblock_ext(g.order:(g.order+blklength-1)),yblock_ext(flip(1:g.order)));
    coef_a(iblock,:) = Amat\yblock(iblock,:)';
    res(iblock,:)    = (yblock(iblock,:)'- Amat*coef_a(iblock,:)')';
end

resMSE = sum((res).^2,2)/blklength;