% Script to generate figures and results for: 
%                                           Project 1: LPC Lossy Compression of Speech Signals
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)

%% Loading signal block from speech
% --- Step 1 ---
[tmp,srate] = audioread(fullfile(pwd,'MLKDream','MLKDream.wav'));

% Selecting a segment predefined
t1 = 11.28*60*srate;
t2 = (11.28+0.365)*60*srate;
signal = tmp(round(t1):round(t2));
signal = signal(1:160*3000);
timevect = (t1 + [1:length(signal)])/(srate);

 % Defining Inputs
input.alphastd      = 2;                        % Scaling factor of the STD to perform thresholding
input.qrate         = 4;                        % Quantization rate
input.plotflag      = 1;                        % Flag to plot or not from the functions
input.order         = 10;                       % Order of the AR and filter. Equivalent to 'l'.
input.npts          = 160;                      % Number of points in each block.
input.qratevect     = [1 2 3 4 5 6 7 8 9 10];   % Vector of quantization rate values to be used for the analysis.
input.alphastd_vect = [1:0.2:4];                % Vector of alpha_std values to be used for the analysis.
input.srate         = srate;                    % Sampling rate

flagtitle           = 0;                        % flag to plot title in the figure. Figures for the report might not need this title.
flagprint           = 0;                        % flag to print figures.

file2savefigs = fullfile(pwd,'figures_report');

if input.plotflag
    % Figure 1
    figure1 = figure('name','I have a dream','Position',[-1347 393 1162 522]);
    plot((t1 + [1:length(signal)])/(srate),signal,'LineWidth',1);
    xlabel('Time (s)','fontsize',20);
    ylabel('dB','fontsize',20);
    axis tight;
    grid on;
    set(get(figure1,'Children'),'Fontsize',17);
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure1_Original_signal'),'-depsc','-tiff');
    end
end

%--------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------------------------------------------------
%%  Encoding code block
% --- Step 2 ---
% 1- Divide the signal in blocks of length 'g.npts' (Default value 160)
% 2- Quantize the signal using L = 2^(input.qrate)-1
% 3- Mean square Error is computed between the original signal and the signals quantized at differents levels

[yqblock,yquantcont,hstep2] = ca1_quantizer(signal,'alphastd',input.alphastd,'qrate',input.qrate,'plotflag',input.plotflag,'npts',input.npts,'srate',input.srate,'t0',t1,'flagtitle',flagtitle); 
% Figure 2
if input.plotflag
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure2_Original_and_Quantized_signal'),'-depsc','-tiff');
    end
end
MSEquant_orig = ((signal - yquantcont(:))'* (signal - yquantcont(:)))/length(signal); 

% Plotting dependence of order and MSE
if input.plotflag
    clear MSEall
    for i = 1: length(input.qratevect )
        for j = 1:length(input.alphastd_vect)
            [~,yquantconttmp] = ca1_quantizer(signal,'alphastd',input.alphastd_vect(j),'qrate',input.qratevect (i),'plotflag',0);
            MSEall(i,j)      = ((signal - yquantconttmp(:))'* (signal - yquantconttmp(:)))/length(signal);
            yquantconttmpALL(i,j,:) = yquantconttmp;
        end
    end
    
    % --- % Figure 3
    % Quantization Rate  Vs Mean Square Error
    fig1 =figure('name','Quantization Rate Vs Mean Square Error');
    plot(input.qratevect ,log(MSEall(:,6)),'LineWidth',3);
    if flagtitle
        title(['Quantization Rate  Vs Mean Square Error ( \alpha_{std}= ' num2str(input.alphastd_vect(6)) ')'],'fontsize',17);
    end
    xlabel('r (bits-per-symbol)','fontsize',20);
    ylabel('log(MSE)','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure3_MSE_vs_Quantization_alpha-2'),'-depsc','-tiff');
    end
    
    % --- % Figure 4
    % Alpha_std Vs Mean Square Error
    fig1 =figure('name','Alpha_std Vs Mean Square Error');
    plot(input.alphastd_vect,log(MSEall(4,:)),'LineWidth',3);
    if flagtitle
        title(['\alpha_{std} Vs Mean Square Error (r = ' num2str(input.qratevect (4)) ')'],'fontsize',17);
    end
    xlabel('\alpha_{std}','fontsize',20);
    ylabel('log(MSE)','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure4_MSE_vs_Alpha_r-4'),'-depsc','-tiff');
    end
    
    % --- % Figure 5
    % MSE Original Signal-Quantized Signal
    fig1 = figure('name',' MSE for alpha_std Vs r');
    imagesc(input.qratevect ,input.alphastd_vect,log(MSEall));
    if flagtitle
        title('MSE Original Signal-Quantized Signal','fontsize',17);
    end
    ylabel('\alpha_{std}','fontsize',20);
    xlabel('r (bits-per-symbol)','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    grid on; axis tight;
    hcoolorbar = colorbar('eastoutside');
    set(get(hcoolorbar,'Label'),'String','log(MSE)');
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure5_MSE_Alpha_r_all_step2'),'-depsc','-tiff');
    end
end
%--------------------------------------------------------------------------
%% --- Step 3 & 4 ---
% Estimating coefficients from ORIGINAL SIGNAL

yblock = ca1_breakinblocks(signal,input.npts);
[coef_a,residuals] = ca1_estimcoef(yblock,'order',input.order);
%--------------------------------------------------------------------------
%% --- Step 5 (Estimating residuals using coefficients)---
% 1- Compute the residuals using the coefficnets a
% 2- Compute  the MSE between the residuals estimdated here and the ones from step  5

res_estim  = ca1_estimresiduals(yblock,coef_a);
resMSE     = sum((res_estim - residuals).^2,2)./input.npts;

%---% Figure 12 
% Figure for step 3, 4 and 5 MSE Residuals estimated in Steps 3,4 - 5
if input.plotflag
    fig1 =figure('name','Residuals_over_block','Position',[-1347 393 1162 522]);
    plot(resMSE);
    if flagtitle
        title('MSE Residuals estimated in Steps 3,4 - 5','fontsize',18);
    end
    hlegend = legend('MSE Residuals estimated in Steps 3,4 - 5');
    xlabel('Block Index','fontsize',20);
    ylabel('dB','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    set(hlegend,'box','off','Fontsize',19);
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure12_MSE_vs_Quantization_alpha-2_step5'),'-depsc','-tiff');
    end
end

%---% Figure 13 
% Figure power od the signal and the residuals
if input.plotflag
    fig1 =figure('name','Power_Residuals_and_signal_over_blocks','Position',[-1347 393 1162 522]);
    plotyy(1:size(residuals,1),rms(residuals,2).^2,1:size(residuals,1),rms(yqblock,2).^2);

    if flagtitle
        title('Power of Residuals and Quantized Signal','fontsize',18);
    end
    hlegend = legend('Power of residuals on each block','Power of the quantized signal on each block');
    xlabel('Block Index','fontsize',20);
    ylabel('dB','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    set(hlegend,'box','off','Fontsize',19);
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure13_Power_Residuals_and_qsignal_over_blocks_step5'),'-depsc','-tiff');
    end
end
%--------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------------------------------------------------

%% Decoding code block
% --- Step 6 ---
% Signal is reconstructed from the coefficients and residuals from step 3,4 and 5
yblock_reconstructed = ca1_arfilter(coef_a,residuals);
yblock_reconstructed_cont = reshape(yblock_reconstructed',[1,length(yblock_reconstructed(:))])';% Straightening block of recons signal
MSE_origsig_estimsig = ((signal - yblock_reconstructed_cont)'*(signal - yblock_reconstructed_cont))/length(signal);

%--------------------------------------------------------------------------
%% --- Step 7 ---
% Signal is reconstructed from the quantized residuals
% Quantizing residuals and plotting them
residuals_cont              = reshape(residuals',[1,length(yblock_reconstructed(:))])';
[resq_blk,resq_cont,hstep7] = ca1_quantizer(residuals_cont,'alphastd',input.alphastd,'qrate',input.qrate,'plotflag',input.plotflag,'srate',input.srate,'t0',t1,'flagtitle',0); 

%---% Figure 6
% Plotting quantized signals if flag
if input.plotflag
    handletmp1 = get(hstep7,'Children'); handletmp2= get(handletmp1(2),'Children'); set(handletmp2(1),'DisplayName','Quantized Residuals');
    handletmp1 = get(hstep7,'Children'); handletmp2= get(handletmp1(2),'Children'); set(handletmp2(2),'DisplayName','Original Residuals');
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure6_Original_and_quantized_Residuals'),'-depsc','-tiff');
    end
end

% Reconstructing signal with quantized noise
yblock_reconstructed_qnoise = ca1_arfilter(coef_a,resq_blk);

% Computing MSE btween original signal and reconstructed with quantized residuals
yblock_reconstructed_qnoise_cont = reshape(yblock_reconstructed_qnoise',[1,length(yblock_reconstructed(:))])'; % Straightening block of reconstructed signal (LISTEN TO THIS SIGNAL)
MSE_origsig_estimsig_qnoise      = ((yblock_reconstructed_qnoise_cont - signal)'*(yblock_reconstructed_qnoise_cont - signal))/length(signal);

% Computing MSE btween original signal and reconstructed with quantized residuals (signal in blocks)
for i = 1:length(input.qratevect )
    for j = 1:length(input.alphastd_vect)
        % Quantizing residuals
        resq_block_tmp = ca1_quantizer(residuals_cont,'alphastd',input.alphastd_vect(j),'qrate',input.qratevect(i),'plotflag',0);
        
        % Reconstructing 
        yblock_recon_qnoise_tmp = ca1_arfilter(coef_a,resq_block_tmp);
        
        % Computing MSE
        y_reconstructed_qnoise_cont_ALL(i,j,:)   = reshape(yblock_recon_qnoise_tmp',[1,length(yblock_reconstructed(:))])';% Straightening block of recons signal
        MSE_origsig_estimsig_qnoise_BLOCK(i,j,:) = sum((yblock_recon_qnoise_tmp - yblock).^2,2)./size(yblock,2); % MSE for signal in blocks        
        clear yblock_recon_qnoise_tmp yblock_recon_qnoise_tmp
    end
end

% MSE for cont signal
MSE_origsig_estimsig_qnoise_CONT = sum(MSE_origsig_estimsig_qnoise_BLOCK,3)/size(MSE_origsig_estimsig_qnoise_BLOCK,3); 

if input.plotflag
    %---% Figure 7
    % Figure 2 step 7: MSE(Original Signal, Reconstructed signal
    % w/quantized noise) computed per blocks VS qrate for alpha = 2
    fig1 =figure('name',' Original Signal - Reconstructed signal w/quantized residuals alpha=2 ','Position',[-1347 393 1162 522]);
    subplot(2,4,[1:3]);
    imagesc([1:size(MSE_origsig_estimsig_qnoise_BLOCK,3)],input.qratevect ,log(squeeze(MSE_origsig_estimsig_qnoise_BLOCK(:,7,:))));
    title('MSE Original signal - Reconstructed signal w/quantized residuals (\alpha_{std} = 2)','fontsize',15)
    %xlabel('Block index','fontsize',20);
    ylabel('r (bits-per-symbol)','fontsize',20);
    hcoolorbar1 = colorbar('westoutside');
    set(get(hcoolorbar1,'Label'),'String','log(MSE)');
    grid on;
    
    h1 = subplot(2,4,4);
    plot(sum(MSE_origsig_estimsig_qnoise_BLOCK(:,7,:),3),input.qratevect ,'LineWidth',3)
    title('Sum of MSE across blocks','fontsize',16)
    set(h1,'Ydir', 'reverse')
    %xlabel('MSE','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    grid on; axis tight;
    
    subplot(2,4,[5:7]);
    imagesc([1:size(MSE_origsig_estimsig_qnoise_BLOCK,3)],input.alphastd_vect ,log(squeeze(MSE_origsig_estimsig_qnoise_BLOCK(4,:,:))));
    title('MSE Original signal - Reconstructed signal w/quantized residuals (r = 4)','fontsize',16)
    xlabel('Block index','fontsize',20);
    ylabel('\alpha_{std} ','fontsize',20);
    hcoolorbar2 = colorbar('westoutside');
    set(get(hcoolorbar2,'Label'),'String','log(MSE)');
    grid on;
    
    h1 = subplot(2,4,8);
    plot(sum(MSE_origsig_estimsig_qnoise_BLOCK(4,:,:),3),input.alphastd_vect ,'LineWidth',3)
    %title('Sum of MSE across blocks','fontsize',15)
    set(h1,'Ydir', 'reverse')
    xlabel('MSE','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure7_MSEblocks_quantized_residuals'),'-depsc','-tiff');
    end
   
    %---% Figure 8
    % MSE between original signal and reconstructed signal with quantized
    % residuals
    fig1 = figure('name',' MSE for alpha_std Vs q_rate with');
    imagesc(input.qratevect ,input.alphastd_vect,log(MSE_origsig_estimsig_qnoise_CONT));
    %title('MSE for \alpha_{std} Vs q_{rate}','fontsize',17)
    title('MSE ','fontsize',17)
    ylabel('\alpha_{std}','fontsize',20);
    xlabel('r (bits-per-symbol)','fontsize',20);
    set(get(fig1,'Children'),'Fontsize',17);
    grid on; axis tight;
    hcoolorbar = colorbar('eastoutside');
    set(get(hcoolorbar,'Label'),'String','log(MSE)');
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure8_MSEblocks_quantized_residuals _alpha_r_step7'),'-depsc','-tiff');
    end
    
end
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% --- Step 8 ----
%  MSE vs qrate for directly quantized signals and signal with quantized
%  residuals
for i = 1: length(input.qratevect)
    for j = 1: length(input.alphastd_vect)
        % Quantizing original signal 
        [~,yquantconttmp]           = ca1_quantizer(signal,'alphastd',input.alphastd_vect(j) ,'qrate',input.qratevect(i),'plotflag',0);
        
        % Computing MSE between original signal and directly quantized signal
         MSE_signal_quant(i,j)      = ((signal - yquantconttmp(:))'* (signal - yquantconttmp(:)))/length(signal);
         
         % Computing MSE between original signal and reconstructed signal
         % with quantized residuals (this is from STEP 7)
         MSE_signal_noisequant(i,j) = ((signal - squeeze(y_reconstructed_qnoise_cont_ALL(i,j,:)))'* (signal - squeeze(y_reconstructed_qnoise_cont_ALL(i,j,:))))/length(signal);
    end
end

if input.plotflag
    %---% Figure 9
    % Figure MSE Original signal - Directly quantized Signal, Reconstructed signal w/quantized residuals) and qrate
    fig9 = figure('name','MSE(Directly quantized Signal, Reconstructed signal w/quantized residuals) and r');
    plot(input.qratevect,log(MSE_signal_quant(:,6)),'LineWidth',3);
    hold on;
    plot(input.qratevect,log(MSE_signal_noisequant(:,6)),'LineWidth',3);
    if flagtitle
        title('MSE Original Signal Quantized - Reconstructed Signal w/Quantized Residuals Vs r (\alpha_{std} = 2)','fontsize',14);
    end
    xlabel('r (bits-per-symbol)','fontsize',20);
    ylabel('log(MSE)','fontsize',20);
    hlegend = legend(' MSE Directly quantized signal (\alpha_{std}=2)',' MSE Reconstructed signal w/quantized residuals (\alpha_{std}=2)');
    set(get(fig9,'Children'),'Fontsize',17);
    set(hlegend,'fontsize',14,'box','off');
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure9_MSE_orig_quantized-residuals_quantized_Vs_r-alpha-2'),'-depsc','-tiff');
    end
    
    %---% Figure 10
    % Figure MSE Original signal - Directly quantized Signal, Reconstructed signal w/quantized residuals) and alpha
    fig10 = figure('name','MSE Directly quantized Signal - Reconstructed signal w/quantized noise) and alpha');
    plot(input.alphastd_vect,log(MSE_signal_quant(4,:)),'LineWidth',3);
    hold on;
    plot(input.alphastd_vect,log(MSE_signal_noisequant(4,:)),'LineWidth',3);
    if flagtitle
        title('MSE Directly quantized Signal - Reconstructed Signal w/Quantized Residuals Vs \alpha_{std}) (r = 4)','fontsize',15);
    end
    xlabel('\alpha_{std}','fontsize',20);
    ylabel('log(MSE)','fontsize',20);
    hlegend = legend([' MSE Directly quantized signal (r=' num2str(input.qratevect(4)) ')'],[' MSE Reconstructed signal w/quantized residuals (r=' num2str(input.qratevect(4))  ')']);
    set(get(fig10,'Children'),'Fontsize',17);
    set(hlegend,'fontsize',14,'box','off');
    grid on; axis tight;
    set(gcf,'PaperPositionMode','auto');
    if flagprint
        print(fullfile(file2savefigs,'Figure10_MSE_orig_quantized-residuals_quantized_Vs_alpha_r-4'),'-depsc','-tiff');
    end
    
end

%--------------------------------------------------------------------------
% %% NOTE: THE FILTER BECOMES UNSTABLE FOR THE QUANTIZED VALUES OF THE COEFFICIENTS AND RESIDUALS
% NOTICE THAT THE VALUES OF THE RECONSRUCTED SIGNALS SEEMS TO BE OK, BUT A
% MORE CLOSE LOOK LEAD TO BELIVE THAT THE RECONSTRUCTION FAILS FOR SEVERAL
% BLOCKS AS VALUES OUT OF TEH RANGE OF THE SIGNAL ARE SPIKING.
%% --- Step 9 ---
%  Quantizing coefficients and residuals

% Quantizing residuals
coef_a_cont    = reshape(coef_a',[1,length(coef_a(:))])';
residuals_cont = reshape(residuals',[1,length(yblock_reconstructed(:))])';
qrate_r_vect   = [4 8 9];

for i = 1: length(qrate_r_vect)
    % Project approach. Quantize acoeff and residuals AFTER compute residuals
    coef_aq_blk{i}   = ca1_quantizer(coef_a_cont,'alphastd',input.alphastd,'qrate',qrate_r_vect(i),'plotflag',0,'npts',input.order);
    resq_blk2{i}     = ca1_quantizer(residuals_cont,'alphastd',input.alphastd,'qrate',qrate_r_vect(i),'plotflag',0,'npts',input.npts);
    
    % Reconstructing signal w/quantized filter coefficients (a) and  residuals;
    yblock_reconstructed_qnoise_qa = ca1_arfilter(coef_aq_blk{i},resq_blk2{i});
    
    % Straightening block of recons signal
    yblock_reconstructed_qnoise_qa_cont(i,:) = reshape(yblock_reconstructed_qnoise_qa',[1,length(yblock_reconstructed_qnoise_qa(:))])';
    
    % computing MSE
    MSE_origsig_estimsig_qa_qnoise(i) = ((signal - yblock_reconstructed_qnoise_qa_cont(i,:)')'*(signal - yblock_reconstructed_qnoise_qa_cont(i,:)'))/length(signal);
    
    % Alternative approach % Quantize acoeff and quantize recomputed
    % residuals (with acoef quantized)
    % Recalculate residuals with acoeff quantized
    res_estim_tmp  = ca1_estimresiduals(yblock,coef_aq_blk{i});
    
    % Quantize residuals
    residuals_cont_tmp      = reshape(res_estim_tmp',[1,length(yblock_reconstructed(:))])';
    resq_blk2_approach2{i}  = ca1_quantizer(residuals_cont_tmp,'alphastd',input.alphastd,'qrate',qrate_r_vect(i),'plotflag',0,'npts',input.npts);

    % Reconstructing signal with quantized noise
    yblock_reconstructed_qnoise_qa_approach2 = ca1_arfilter(coef_aq_blk{i},resq_blk2_approach2{i});
    
    % Straightening block of recons signal
    yblock_reconstructed_qnoise_qa_approach2_cont(i,:) = reshape(yblock_reconstructed_qnoise_qa_approach2',[1,length(yblock_reconstructed_qnoise_qa(:))])';
    
    MSE_origsig_estimsig_qa_qnoise_approach2(i) = ((signal - yblock_reconstructed_qnoise_qa_approach2_cont(i,:)')'*(signal - yblock_reconstructed_qnoise_qa_approach2_cont(i,:)'))/length(signal);
end