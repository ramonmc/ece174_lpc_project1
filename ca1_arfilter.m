% ca1_arfilter() - Contains proccesing described in Step 6 of the computer
% assignment. Estimated a signal from the coefficients of the LPC and the
% residuals
%
% Input:
%   coef_a    - Coefficients. Dimension [Number of blocks, order]
%   res       - Residuals between the signal reconstructed with the
%
% Optional input:
%
%
% Outputs:
%   yhat         - Estimated signal in blocks
%
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function yhat = ca1_arfilter(coef_a,residuals)

[g.nblocks,g.order] = size(coef_a);
g.npts = size(residuals,2);
 
 yhat = zeros(size(residuals));

for iblock = 1:g.nblocks
    for ipts = 1:g.npts
        if ipts < g.order+1
            if iblock == 1
                if ipts == 1
                    yhat(iblock,ipts) = 0 + residuals(iblock,ipts);
                else
                    yhat(iblock,ipts) = coef_a(iblock,:)*flip([zeros(1,g.order-ipts+1) yhat(iblock,1:ipts-1)])'+ residuals(iblock,ipts);
                end
            else
                yhat(iblock,ipts) = coef_a(iblock,:)*flip([yhat(iblock-1,(end-(g.order-ipts)):end) yhat(iblock,1:ipts-1)])'+ residuals(iblock,ipts);
            end
        else
            yhat(iblock,ipts) = coef_a(iblock,:)* yhat(iblock,flip((ipts-g.order):ipts-1))'+ residuals(iblock,ipts);
        end
    end
end