% ca1_estimresiduals() - Estimate residuals using eq 2 in sliding windows (See computer assigment 1).
%
% Input:
%   yblock    - Qunatized signal [Dimension [Number of blocks,Number of points ]
%   coef_a    - Coefficients. Dimension [Number of blocks, order]
%   res       - Residuals between the signal reconstructed with the
%                coefficients and the quantized signal (input) in the way
%                res = y_orig- A*coef_a
%
% Outputs:
%   res_estim - Residuals between the signal reconstructed with the
%               coefficients and the quantized signal (input) using sliding
%               windows
%
%
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function [res_estim, resMSE] = ca1_estimresiduals(yblock,coef_a)
%CA1_ESTIMRESIDUALS Summary of this function goes here
%   Detailed explanation goes here
g.order = size(coef_a,2);
g.npts  = size(yblock,2);
yhat = zeros(size(yblock));
for iblock = 1:size(yblock,1) 
    for ipts = 1:g.npts
        if ipts < g.order+1
            if iblock == 1
                if ipts == 1
                    yhat(iblock,ipts) = 0;
                else
                    yhat(iblock,ipts) = coef_a(iblock,:)*flip([zeros(1,g.order-ipts+1) yblock(iblock,1:ipts-1)])';
                end
            else
                yhat(iblock,ipts) = coef_a(iblock,:)*flip([yblock(iblock-1,(end-(g.order-ipts)):end) yblock(iblock,1:ipts-1)])';
            end
        else
            yhat(iblock,ipts) = coef_a(iblock,:)* yblock(iblock,flip((ipts-g.order):ipts-1))';
        end
    end
end

res_estim = yblock - yhat;
end