% ca1_quantizer() - Contains proccesing described in Step 2 of the computer assignment.
%
% Input:
%   signal       - input signal
%
% Optional input:
%   npts         - Number of points on each segment. Default [160]
%   qrate        - Quantization rate(bits/sec). Default [2]
%   srate        - Sampling rate
%   t0           - Initial time for the signal
%   plotflag     - Plot thresholded signal and activate plots. Default [0] (no plot)
%   alphastd     - Value that multiply the STD to dtermine te thresold values. Default [2]
%
% Outputs:
%   yq_block         - Quantized signal in blocks
%   MSE              - Mean Suqered error between the original signal and the quantized
%   yquantcont       - Continuous quantized signal;
%   h                - Figure handles 
%
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function [yq_block,yquantcont,h] = ca1_quantizer(signal,varargin)
yq_block = []; signal = signal(:); h = {};

try
    options = varargin;
    if ~isempty( varargin ),
        for i = 1:2:numel(options)
            g.(options{i}) = options{i+1};
        end
    else g= []; end;
catch
    disp('ca1_quantizer() error: calling convention {''key'', value, ... } error'); return;
end;
try g.npts;                  catch, g.npts         = 160;   end;
try g.srate;                 catch, g.srate        = [];    end;
try g.t0;                    catch, g.t0           = [];    end;
try g.plotflag;              catch, g.plotflag     = 0;     end;
try g.alphastd;              catch, g.alphastd     = 2;     end;
try g.qrate;                 catch, g.qrate        = 1;     end;
try g.flagtitle;             catch, g.flagtitle    = 1;     end;

if ~isempty(g.srate)
    timevect = (g.t0 + [1:length(signal)])/(g.srate);
else
    timevect = 1:length(signal);
end

% Breaking signal in blocks
y_block = ca1_breakinblocks(signal,g.npts);

% Thresholding signal
ymean = mean(y_block,2);
ystd  = std(y_block')';
y_block_thres = y_block;

for iblock = 1:size(y_block,1)
    posindx = find(((y_block(iblock,:)-repmat(ymean(iblock),1,size(y_block,2))) >=  g.alphastd*ystd(iblock)));
    negindx = find(((y_block(iblock,:)-repmat(ymean(iblock),1,size(y_block,2))) <= -g.alphastd*ystd(iblock)));
    
    y_block_thres(iblock,posindx) =  ymean(iblock) + g.alphastd*ystd(iblock);
    y_block_thres(iblock,negindx) =  ymean(iblock) - g.alphastd*ystd(iblock);
end

%% Plot thresholded signal (for developer)
% if g.plotflag
%     h{1} = figure('name','Thresholded Signal');
%     plot(signal); hold on;
%     plot(reshape(y_block_thres',[1,length(y_block_thres(:))])); 
%     leg = legend('Original Signal','Thresholded Signal');
%     set(leg,'fontsize',14, 'box', 'off');
%     title(['Thresholded Signal (Alpha_{std} =' num2str( g.alphastd) ')'],'fontsize',16);
%     xlabel('Tims (s)','fontsize',14);
% %     ylabel('MSE','fontsize',14);
%     grid on;
%     axis tight;
% end
%%
% Computing y quantized
L = 2^g.qrate-1;
q = (max(y_block_thres,[],2)-min(y_block_thres,[],2))/L;
yq_block =round(y_block_thres./repmat(q,1,size(y_block_thres,2))).*repmat(q,1,size(y_block_thres,2));

% Building up the quantized signal
yquantcont = reshape(yq_block',[1,length(y_block_thres(:))])';
paddim = length(yq_block(:))-length(signal);
yquantcont = yquantcont(1:end-paddim);

% MSE = ((signal - yquantcont(:))'* (signal - yquantcont(:)))/length(signal); 

if g.plotflag
    h = figure('name','Quantized Signal','Position',[-1347         393        1162         522]);
    plot(timevect,signal); hold on;
    plot(timevect,yquantcont);
    if g.flagtitle
        title(['Original and Quantized  Signal (\alpha_{std}= ' num2str( g.alphastd) ', r = ' num2str(g.qrate) ')'],'fontsize',20,'FontWeight','Bold');
    end
    leg = legend('Original Signal','Quantized Signal');
    set(leg,'fontsize',10, 'box', 'on','FontWeight','Bold','LineWidth',0.1,'EdgeColor', [1 1 1 ]);
    set(get(leg,'BoxFace'), 'ColorType','truecoloralpha', 'ColorData',uint8(255*[1;1;1;.7]));
    xlabel('Time(s)','fontsize',20);
    ylabel('dB','fontsize',20);
    set(get(h,'Children'),'Fontsize',17)
    grid on;
    axis tight;
end