% ca1_breakinblocks() - Break seignal in blocks of n points(See computer assigment 1).
%
% Input:
%   y      - Signal Dimension [1Number of points ]
%  npts    - Points in each block
%
% Outputs:
%   yblock    - Signal breaken down in blocks. Dimension [Number of blocks, npts]
%
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function yblock = ca1_breakinblocks(y,npts);
if length(y) < npts
    display('ca1_breakinblocks : Ivalid inputs. Lengths of blocks should be smaller than the signal');  
    return;
end

nblock = ceil(length(y)/npts);
yblock = zeros(nblock, npts);

n = 1;
for i = 1:nblock
    if i~=nblock
        yblock(i,:) = y(n:n+npts-1);
    else
        yblock(i,1:length(y(n:end))) = y(n:end);
    end
    n = n+npts;
end